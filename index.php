<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<body class="container">

<ul class="nav nav-tabs mb-4">
    <li class="nav-item">
        <a class="nav-link active" id="btn-tab-1" href="#" onclick="changeTab(1)">Data Mahasiswa</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="btn-tab-2" href="#" onclick="changeTab(2)">Tab 2</a>
    </li>
</ul>

<div class="d-block" id="tab-1">

    <h1>Data Mahasiswa</h1>
    <form action="form.php" method="post">
        <table>
            <tr valign="top">
                <td><label for="field-nim">NIM :</label></td>
                <td><input class="form-control" type="number" id="field-nim" name="nim"></td>
            </tr>
            <tr valign="top">
                <td><label for="field-name">Nama :</label></td>
                <td><input class="form-control" type="text" id="field-name" name="name"></td>
            </tr>
            <tr valign="top">
                <td><label for="field-birthdate">Tgl Lahir (dd-mm-yyyy) :</label></td>
                <td><input class="form-control" type="date" name="birthdate" id="field-birthdate"></td>
            </tr>
            <tr valign="top">
                <td><label for="field-gender">Jenis Kelamin :</label></td>
                <td>
                    <input class="form-check-input" type="radio" name="gender" value="pria" id="field-gender" checked> Laki-laki <br>
                    <input class="form-check-input" type="radio" name="gender" value="wanita" id="field-gender"> Perempuan
                </td>
            </tr>
            <tr valign="top">
                <td><label for="field-address-bandung">Alamat di Bandung :</label></td>
                <td><textarea class="form-control" name="address_bandung" id="field-address-bandung" cols="25" rows="5"></textarea></td>
            </tr>
            <tr valign="top">
                <td><label for="field-category-address">Tinggal di :</label></td>
                <td>
                    <select class="form-control" name="address_category" id="field-category-address">
                        <option value="">--pilih--</option>
                        <option value="kos">Kos/Kontrakan</option>
                        <option value="saudara">Rumah Saudara</option>
                        <option value="orangtua">Rumah Orang Tua</option>
                    </select>
                </td>
            </tr>
            <tr valign="top">
                <td><label for="field-hobby">Pilih 2 hobi yang paling disukai : &nbsp;&nbsp; </label></td>
                <td>
                    <input class="form-check-label" type="checkbox" name="hobby[]" id="field-hobby" value="science"> Science <br>
                    <input class="form-check-label" type="checkbox" name="hobby[]" id="field-hobby" value="technology"> Technology <br>
                    <input class="form-check-label" type="checkbox" name="hobby[]" id="field-hobby" value="art"> Art <br>
                    <input class="form-check-label" type="checkbox" name="hobby[]" id="field-hobby" value="sport"> Sport <br>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-warning">Reset</button>
                </td>
                <td></td>
            </tr>
        </table>
    </form>

    <div class="content position-relative d-none">
        <div class="card position-absolute" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title"><a href="#" id="apb-jurnal-mod-4"
                                          onmouseover="showDetail('abp-jurnal-mod-4-detail')"
                                          onmouseleave="hideDetail('abp-jurnal-mod-4-detail')">Aplikasi Berbasis Platform</a></h5>
                <h6 class="card-subtitle mb-2 text-muted">Jurnal Modul 4</h6>
                <p class="card-text">Berdoalah sebelum mengerjakan dan mengumpulkan</p>
            </div>
        </div>
        <div class="card position-absolute m-5 d-none" id="abp-jurnal-mod-4-detail" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Aplikasi Berbasis Platform</h5>
                <h6 class="card-subtitle mb-2 text-muted">Jurnal Modul 4</h6>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                    card's content.</p>
            </div>
        </div>
    </div>
</div>

<div class="d-none" id="tab-2">
    Konten Bebas
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script src="./javascript/script.js"></script>
</body>

</html>